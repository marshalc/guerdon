# Project Guerdon
This is an application to help manage holidays (aka: leave entitlement) for the Nuffield Department of Surgical Sciences.

See the [main documentation](docs/index.rst) for more information