#!/usr/bin/python
# coding: utf-8
import os

from django.conf import settings
from django.test import LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from sauceclient import SauceClient
from selenium import webdriver


class ProcurementTestCase(StaticLiveServerTestCase):
    def setUp(self):
        # this is how you set up a test to run on Sauce Labs
        username = settings.SAUCELABS_USER
        try:
            key = os.environ["SAUCE_ACCESS_KEY"]
        except KeyError:
            key = settings.SAUCELABS_KEY
        # print("DEBUG: [{0}] and [{1}]".format(key, settings.ON_TRAVIS_CI))
        if key != '' and settings.ON_TRAVIS_CI:
            desired_cap = {
                'platform': "Mac OS X 10.11",
                'browserName': "firefox",
                'tunnel-identifier': os.environ["TRAVIS_JOB_NUMBER"],
                "build": os.environ["TRAVIS_BUILD_NUMBER"],
                "tags": [os.environ["TRAVIS_PYTHON_VERSION"], "CI"]
            }

            self.browser = webdriver.Remote(
                command_executor='http://{0}:{1}@localhost:4445/wd/hub'.format(username, key),
                desired_capabilities=desired_cap
            )
            self.sauce_client = SauceClient(username, key)
        else:
            self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(10)

    def tearDown(self):
        # If all goes well, and this is remote, update...
        try:
            self.sauce_client.jobs.update_job(self.browser.session_id, passed=True)
        except AttributeError:
            pass
            # AttributeError: 'ProcurementTestCase' object has no attribute 'sauce_client'

        self.browser.quit()
        # pass

    def test_access_admin(self):
        base_uri = 'http://localhost:4445/wd/hub' if settings.ON_TRAVIS_CI else self.live_server_url
        print("DEBUG: TESTING: base_uri={0}".format(base_uri))

        # Visit the website
        home_page = self.browser.get(base_uri + '/admin/')
        brand_element = self.browser.find_element_by_id('site-name')
        self.assertEqual('Django administration', brand_element.text)


    def test_external_access(self):
        # EXTERNAL TESTING
        # self.browser.implicitly_wait(10)
        self.browser.get("http://www.google.com")
        if "Google" not in self.browser.title:
            raise Exception("Unable to load google page!")
        # elem = self.browser.find_element_by_name("q")
        # elem.send_keys("Sauce Labs")
        # elem.submit()
        # print(self.browser.title)



    # Alice locates the holiday booking website
    # Alice logs into the website
    # Alice can see how many days of leave she has available
    # Alice enters the start and end dates for time off
    # Alice can see how many days are being requested
    # Alice is told her request is now pending
    # David is alerted to Alice's request
    # David authorises Alice's request
    # Alice is notified of the request confirmation
    # Alice is notified of how many days of leave she has remaining
