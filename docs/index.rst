.. Guerdon documentation master file

Documentation for Project Guerdon
=================================

This is an application to help manage holidays (aka: leave entitlement) for the Nuffield Department of Surgical
Sciences. This has dual aims of becoming a productive system for the department, and also as an exemplar project using
TDD and Document First approaches.

You should find information related to the overall system from design, through to deployment and
maintenance. If you have any questions, please contact the main author Carl Marshall
(carl.marshall@nds.ox.ac.uk).

Quick links:

.. * Production system: https://cope.nds.ox.ac.uk/

* Test system: https://guerdon.cm13.net/  <--  Not yet live!
* Code repository: https://github.com/marshalc/guerdon
* Issue tracking: https://github.com/marshalc/guerdon/issues

Contents
--------

.. toctree::
   :maxdepth: 2

   _source/introduction
   _source/requirements
   _source/user_stories
   _source/development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Note on documentation
=====================

This documentation was assembled using Sphinx, with a combination of Markdown based files for the majority
of the prose, intermingled with ReST for using Sphinx macros and autodoc extensions.

As ever, these documents are a work in progress.
