# Requirements

## Raw notes:

A staff member contacts their line manager to request dates off (don’t want all team off at one time). A form is filled out that is nominally counter-signed by manager and then passed onto admin. Staff member can query how much time is remaining in their leave. Staff member can cancel their leave (mustn’t lose record of it - and a reason code recorded) though when they do it (before or after) can vary.

Part-time staff members have working patterns, and the system knows it (and can be user edited - but can’t change their overall number of hours - admin can change their overall). Ideally the system can calculate their remaining leave. Pattern changes need managerial approval. 

There may be more than one manager, all of whom should informed, and one or more can say yes (for simplicity one manager has to say yes - and we’ll remember who that is). Managers will want an overview of their own staff leave calendar for a quick reprisal. Perhaps the system can point out when there are multiple leaving.

Admin would like an overview on untaken leave, and other potential reports. Add, disable (never delete), review multiple years of leave (such as filter for visa holders - who need 5 years worth of holiday dates). Report on individuals and groups. Log other types of leave - e.g. compassionate leave, unpaid leave, emergency leave (private to admin), jury service. Admin would like a report on working pattern changes. Want to group people, though people can belong to more than one group. 

Nurses for example work longer hours days (e.g. 9 hour days) but fewer days per week. Where they work less than 100% FTE, calculate using part-time rules (where a bank-holiday day is 7.5 hours, not 9h).

People can have two jobs - at different grades - in the same department.

Part-time people get recorded in hours, whereas full-time in days and/or half-days. There’s a spreadsheet that does the calculation (request it). Spreadsheet is updated each year.

Variable hours people have zero hour contracts, and do work as and when required. They get a timesheet and are paid accordingly. That time accrues leave time incrementally up to 28 days per annum (inc bank holidays). Can collect their timesheet data (Margaux handles these mostly), and therefore can export their timesheets for the EU projects.

Things needed to calculate:

- Grade: 1-5 have 7.3h working days; 6+ have 7.5h days
- (30 + long-service + bank-holidays(8) + carry-over-days) * hours-in-a-day * %FTE = Total entitlement
- Then the number of months in a year as per contract
- Then work which bank holiday days to subtract their hours from based on working pattern

SITU have their own group calendar and sub process. Consider sending updates to their calendar.

Leave entitlement needs to be editable by the admin team.

Users should only see their own leave. Managers can see group’s leave. Admin can see all leave. + Luke and Catherine (add them to admin group).

Admin want to know when contract starts, and ends. Need people to be able to leave, and resume. Carry over can be positive or negative. Need to specify bank holiday dates. No mandatory closure days. Would like to be able to make private notes against people (e.g. notice being handed in).

Sick leave is next on the wish list.

## Non-Functional requirements

A list of thoughts related to the implementation:

* Brand it with NDS logo and styling where possible, so that it can blend into existing systems as much as possible
* Develop with familiar technologies: Python, Django, etc
* Authentication will need to tie into the university SSO system eventually
* Make it mobile device friendly, as this is something that staff will want to do from anywhere, and any when.
* Email notifications with direct action links will be wanted to help speed up manager actions (since many will not want to log in and check a website)