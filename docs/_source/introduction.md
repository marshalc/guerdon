# Project Guerdon
This is an application to help manage holidays (aka: leave entitlement) for the Nuffield Department of Surgical Sciences.

## Project Name: Guerdon
[Thesaurus ideas](http://www.thesaurus.com/browse/entitlement?s=t) for *entitlement* come back with a range of suggestions, from which I selected:
[Guerdon](http://www.oxforddictionaries.com/definition/english/guerdon) - A reward or recompense
> 	**Origin**
> 	Late Middle English: from Old French, from medieval Latin widerdonum, alteration (by association with Latin donum 'gift') of a West Germanic compound represented by Old High German widarlōn 'repayment'.

## Outline
**Name**: Guerdon

**Goal**: A web based system for recording leave requests, authorisation, and providing and administrative overview of leave within the department for reporting purposes.

**Metrics**: Staff members can request, cancel, review their annual leave. Managers can approve/disapprove, review their group's leave. Admin can create, dis/enable users, review departmental leave. All via one online system.

Also serving as an example project of TDD methodology for Django application development.

**Target Group**: All members of staff in the NDSS



## Documents

* [Developent Environment](development.md)
* [Project Requirements](requirements.md)
* [User Stories](user_stories.md)