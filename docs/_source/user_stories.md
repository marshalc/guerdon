User Stories
======
The collection of stories that underpin the expected behaviour of this application. Drawn from interviews with potential system users (e.g. Admin team)

## Background

Working within current NDS staff scenarios and University policy, the following roles/types of user have been identified:

* **FTE (Full Time Equivalent) Staff Member** (*Alice*): Most staff fall into this category. May be on a Permanent or Temporary Contract with the department
* **%FTE (Part-time) Staff Member** (*Bob*): These are staff members with a set number of hours per week that is less than full-time hours.
* **ZeroHours (Contractor) Staff Member** (*Carol*): These are ad-hoc employees who are called to work on demand (e.g. transplant nurses)
* **Line Managers** (*David*): Are also staff members, but are responsible for authorising leave for specified other staff members
* **Administrators** (*Elaine*): Are also staff members, and responsible for recording and administering all leave across the department

There are one or two more complicated users, for example:

* Staff who have more than one Line Manager
* Staff who work more than one %FTE contract within the department

### In general

* All staff are expected to book time off in day, or half-day increments. 
* However, whilst FTE and %FTE staff have a preset number of days leave entitlement, ZeroHours staff have to build theirs up during their course of work. 
* %FTE and ZeroHours staff have their entitlement calcuated in hours, and then rounded down to the nearest half day.

### Entitlement calculations

It is expected that the system can calculate a member's holiday entitlement for each holiday period (which runs 1st October to 30th September) based on University rules. There are occasions when these rules will need to be flexed at departmental discretion, so some flexibility is required.

Calculating entitlement can be summarised as:

* %FTE and ZeroHour Staff member's grade
  * Grades 1-5 have 7.3h working days; 
  * Grades 6+ have 7.5h working days. 
* FTE is equivalent to 5 * hours-per-working-day, so: 
  * Grades 1-5 = 36.5hours per week
  * Grades 6+ = 37.5hours per week
* The entitlement period defaults to 1st Oct through to end of 30th September
  * Unless your contract ends, or is changed (e.g. FTE to %FTE) before 30th Sept
  * And/or your contract starts after 1st October
* Bank holidays are assumed leave days, and awarded based on the dates that fall within your entitlement period
  * QUERY: What happens if not taken as a leave day?
  * For %FTE staff, the equivalent of 1 normal working day (7.3 or 7.5 hours) is subtracted if their working pattern involves them working on a bank holiday (and they are assumed to be on leave that day, regardless of how long their pattern expects them to work)
* FTE and %FTE staff have:
  * A base of 30 days per year entitlement
    * This is reduced proportionally to the length of the entitlement period compared to the year
  * 0-5 extra long-service days per year
  * A number of carry-over days from previous entitlement period
    * These can be positive, or negative, to allow for adjustments as required.
    * E.g. 5 days of leave left over at the end of one period, carry over to the next
    * E.g. An FTE member working all year who takes 15 out of 20 days in the first 6 months of their period, but who then changes to a 50%FTE contract, would have used 5 days more pro-rata than allowed, and thus would have their next entitlement period reduced by those 5 days (and therefore 36.5 or 37.5 hours less)

Thus the overall calculation looks like:

`(Base entitlement + Long service + carry-over + bank holidays) * Hours in working day * %FTE ==> Rounded down to nearest half-day` = Days Awarded

## Personas

Some background technique can be found at [http://www.romanpichler.com/blog/10-tips-writing-good-user-stories/]()

**Persona**: *Alice* is a member of the Bone Oncology group, with a Full Time permanent contract on Grade 7, where she has worked for 7 years. She books occasional days off at short notice because of children, and takes a long vacation in the summer.

**Persona**: *David* is the head of the Bone Oncology group, and manages *Alice* amongst 13 other members of staff. He reports to the Head of Department, and has been working at Oxford for 20 years. David relies on his PA to handle most of the adminstration, but signs off on holiday requests in batches once a week. He can often be away from his office for a week or more at time with conference and research meetings, so is used to working remotely.

**Persona**: *Bob* is PA to *David* in the Bone Oncology group, and works 60% FTE (typically a Mon, Wed, Fri pattern), and has been in the role for 2 years, but his contract ends 8 months into the acadmic year. 

**Persona**: *Carol* is a research nurse that helps support a project run by *Alice*. She is called in to work as the situation demands, but is also a student, so likes to take time off in the vacations when she can.

**Persona**: *Elaine* is part of the HR admin team, and responsible for compiling reports on departmental leave status, and handling queries over allocations and times.

## Epics

1. Alice wants to book two weeks off over the 2016 August Bank Holiday weekend
2. Alice wants to book a day off on Feb 29th, 2016
3. Alice changes her mind, and cancels the Feb 29th holiday
4. Alice wants to book April 21st to April 30th off. David rejects the request as Alice is need in on the last week of April.
5. Bob wants to book April 21st to April 30th off. But he returns to work on the 30th and wants to reclaim the day's leave.

## Sprints
1. FTE staff can book holidays
2. FTE staff holidays can be amended/rejected
3. %FTE staff can book holidays

## Ready Stories

From Epic #1:

1. Alice locates the holiday booking website
2. Alice logs into the website
3. Alice can see how many days of leave she has available
3. Alice enters the start and end dates for time off
4. Alice can see how many days are being requested
5. Alice is told her request is now pending
6. David is alerted to Alice's request
7. David authorises Alice's request
8. Alice is notified of the request confirmation
9. Alice is notified of how many days of leave she has remaining